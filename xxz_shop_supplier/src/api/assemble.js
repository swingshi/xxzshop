import request from '@/utils/request'

let AssembleApi = {


 /*拼团活动列表*/
 activeList(data, errorback) {
   return request._post('/supplier/activity.assemble/index', data, errorback);
 },
 getProduct(data, errorback){
    return request._get('/supplier/activity.assemble/add', data, errorback);
 },
 addProduct(data, errorback){
    return request._post('/supplier/activity.assemble/add', data, errorback);
 },
 /*我的活动列表*/
 myList(data, errorback) {
   return request._post('/supplier/activity.assemble/my', data, errorback);
 },
 /*根据主键查询*/
 detailProduct(data, errorback){
    return request._get('/supplier/activity.assemble/edit', data, errorback);
 },
 /*根据主键查询*/
 saveProduct(data, errorback){
    return request._post('/supplier/activity.assemble/edit', data, errorback);
 },
 delProduct(data, errorback){
     return request._post('/supplier/activity.assemble/del', data, errorback);
 },
}
export default AssembleApi;
