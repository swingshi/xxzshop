<?php

namespace app\api\controller\order;

use app\api\model\order\Cart as CartModel;
use app\api\model\order\Order as OrderModel;
use app\api\service\order\settled\MasterOrderSettledService;
use app\api\controller\Controller;
use app\api\model\settings\Message as MessageModel;
use app\common\enum\order\OrderTypeEnum;
use app\common\enum\settings\SettingEnum;
use app\common\library\helper;
use app\api\model\settings\Setting as SettingModel;
use app\common\model\app\AppOpen;
use app\common\model\app\AppOpen as AppOpenModel;
use app\common\model\settings\Setting;

/**
 * 普通订单
 */
class Order extends Controller
{
    /**
     * 订单确认-立即购买
     */
    public function buy()
    {
        // 立即购买：获取订单商品列表
        $params = json_decode($this->postData()['params'], true);

        $supplierData = OrderModel::getOrderProductListByNow($params);
        $user = $this->getUser();
        // 实例化订单service
        $orderService = new MasterOrderSettledService($user, $supplierData, $params);
        // 获取订单信息
        $orderInfo = $orderService->settlement();
        // 订单结算提交
        if ($orderService->hasError()) {
            return $this->renderError($orderService->getError());
        }
        if ($this->request->isGet()) {
            // 如果来源是小程序, 则获取小程序订阅消息id.获取支付成功,发货通知.
            $template_arr = MessageModel::getMessageByNameArr($params['pay_source'], ['order_pay_user', 'order_delivery_user']);
             //是否显示店铺信息
            $store_open=SettingModel::getItem('nav')['data']['list'][2]['is_show']?1:0;
            // 是否开启h5支付宝支付
            $h5_alipay = $params['pay_source'] == 'h5' && $this->isH5AlipayOpen();
            $app_alipay = $params['pay_source'] == 'app' && $this->isAppAlipayOpen($user['app_id']);
            $balance = $user['balance'];
            return $this->renderSuccess('', compact('orderInfo', 'template_arr', 'store_open', 'h5_alipay' , 'app_alipay','balance'));
        }
        // 创建订单
        $order_arr = $orderService->createOrder($orderInfo);
        if(!$order_arr){
            return $this->renderError($orderService->getError() ?: '订单创建失败');
        }
        // 构建支付请求
        $payment = OrderModel::onOrderPayment($user, $order_arr, $params['pay_type'], $params['pay_source']);

        // 返回结算信息
        return $this->renderSuccess(['success' => '支付成功', 'error' => '订单未支付'], [
            'order_id' => helper::getArrayColumn($order_arr, 'order_id'),   // 订单id
            'pay_type' => $params['pay_type'],  // 支付方式
            'payment' => $payment,               // 微信支付参数
            'order_type' => OrderTypeEnum::MASTER, //订单类型
        ]);
    }

    /**
     * 订单确认-立即购买
     */
    public function cart()
    {
        // 立即购买：获取订单商品列表
        if ($this->request->isGet()) {
            $params = json_decode($this->postData()['params'], true);
        }else{
            $params = json_decode($this->postData()['params'], true);
        }
        $user = $this->getUser();
        // 商品结算信息
        $CartModel = new CartModel($user);
        // 购物车商品列表
        $supplierData = $CartModel->getList($params['cart_ids']);
        // 实例化订单service
        $orderService = new MasterOrderSettledService($user, $supplierData, $params);
        // 获取订单信息
        $orderInfo = $orderService->settlement();
        if ($this->request->isGet()) {
            // 如果来源是小程序, 则获取小程序订阅消息id.获取支付成功,发货通知.
            $template_arr = MessageModel::getMessageByNameArr($params['pay_source'], ['order_pay_user', 'order_delivery_user']);
             //是否显示店铺信息
            $store_open=SettingModel::getItem('nav')['data']['list'][2]['is_show']?1:0;
            // 是否开启h5支付宝支付
            $h5_alipay = $params['pay_source'] == 'h5' && $this->isH5AlipayOpen();
            $app_alipay = $params['pay_source'] == 'app' && $this->isAppAlipayOpen($user['app_id']);
            $balance = $user['balance'];
            return $this->renderSuccess('', compact('orderInfo', 'template_arr','store_open', 'h5_alipay','balance'));
        }
        // 订单结算提交
        if ($orderService->hasError()) {
            return $this->renderError($orderService->getError());
        }
        // 创建订单
        $order_arr = $orderService->createOrder($orderInfo);
        if(!$order_arr){
            return $this->renderError($orderService->getError() ?: '订单创建失败');
        }
        // 移出购物车中已下单的商品
        $CartModel->clearAll($params['cart_ids']);
        // 构建支付请求
        $payment = OrderModel::onOrderPayment($user, $order_arr, $params['pay_type'], $params['pay_source']);
        // 返回结算信息
        return $this->renderSuccess('', [
            'order_id' => helper::getArrayColumn($order_arr, 'order_id'),   // 订单id
            'pay_type' => $params['pay_type'],  // 支付方式
            'payment' => $payment,               // 微信支付参数
            'order_type' => OrderTypeEnum::MASTER, //订单类型
        ]);
    }

    /**
     * 是否开启h5支付宝支付
     */
    private function isH5AlipayOpen(){
        return Setting::getItem(SettingEnum::H5ALIPAY)['is_open'];
    }

    /**
     * 是否开启app支付宝支付
     */
    private function isAppAlipayOpen($app_id){
        $config = AppOpenModel::getAppOpenCache($app_id);
        return $config['is_alipay'] == 1?true:false;
    }
}