<?php

namespace app\api\model\plus\live;

use app\api\model\user\Grade as GradeModel;
use app\api\service\order\PaymentService;
use app\api\service\order\paysuccess\type\GradePaySuccessService;
use app\common\enum\order\OrderPayTypeEnum;
use app\common\enum\order\OrderTypeEnum;
use app\common\exception\BaseException;
use app\common\model\plus\live\Plan as PlanModel;
/**
 * 礼物模型
 */
class Plan extends PlanModel
{

    /**
     * 获取充值套餐列表
     */
    public function getList()
    {
        return $this->where('is_delete', '=', 0)
            ->order(['sort' => 'asc', 'create_time' => 'asc'])
            ->select();
    }
}