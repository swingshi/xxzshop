<?php

namespace app\shop\controller\plus\task;

use app\shop\controller\Controller;
use app\shop\model\plus\task\Task as TaskModel;
use app\shop\model\plus\task\TaskHelp as TaskHelpModel;

/**
 * 砍价记录控制器
 */
class Task extends Controller
{
    /**
     * 砍价任务列表
     */
    public function index()
    {
        $model = new TaskModel;
        $search = $this->postData('search/s');
        $list = $model->getList($search);
        return $this->renderSuccess('', compact('list'));
    }

    /**
     * 砍价榜
     */
    public function help($task_id)
    {
        $task_id = $this->postData('task_id/i');
        $model = new TaskHelpModel;
        $list = $model->getList($task_id);
        return $this->renderSuccess('', compact('list'));
    }

    /**
     * 删除砍价任务
     */
    public function delete()
    {
        $task_id = $this->postData('task_id/i');
        $model = new TaskModel;
        if (!$model->setDelete(['task_id' => $task_id])) {
            return $this->renderError($model->getError() ?: '删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

}