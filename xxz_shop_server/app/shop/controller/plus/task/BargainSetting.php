<?php

namespace app\shop\controller\plus\task;


use app\shop\controller\Controller;
use app\shop\model\plus\task\BargainSetting as SettingModel;

/**
 * 砍价设置
 */
class BargainSetting extends Controller
{
    /**
     * 拼团设置
     */
    public function edit()
    {
        $model = new SettingModel;
        $data = $this->postData();
        if ($model->edit('basic', $data)) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError($model->getError() ?: '更新失败');
    }

    public function detail()
    {
        $values = SettingModel::getItem('basic');
        return $this->renderSuccess('', compact('values'));

    }

}